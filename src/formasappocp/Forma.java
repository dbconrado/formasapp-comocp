package formasappocp;

import java.awt.Graphics;

/**
 *
 * @author daniel.conrado
 */
public abstract class Forma {
    public abstract void desenhar(Graphics g);
}
