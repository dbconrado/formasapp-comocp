package formasappocp;

/**
 *
 * @author daniel.conrado
 */
public class FormasAppOCP extends javax.swing.JFrame {

    private int xInicial;
    private int yInicial;

    /**
     * Creates new form FormasApp
     */
    public FormasAppOCP() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        rbCirculo = new javax.swing.JRadioButton();
        rbRetangulo = new javax.swing.JRadioButton();
        rbTriangulo = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tela1 = Tela.fabricarTela();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(700, 500));
        setSize(new java.awt.Dimension(400, 300));

        buttonGroup1.add(rbCirculo);
        rbCirculo.setSelected(true);
        rbCirculo.setText("Círculo");
        jPanel1.add(rbCirculo);

        buttonGroup1.add(rbRetangulo);
        rbRetangulo.setText("Retângulo");
        jPanel1.add(rbRetangulo);

        buttonGroup1.add(rbTriangulo);
        rbTriangulo.setText("Triângulo");
        jPanel1.add(rbTriangulo);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jLabel1.setText("Clique, arraste e solte para desenhar a forma escolhida.");
        jPanel2.add(jLabel1);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        tela1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tela1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tela1MouseReleased(evt);
            }
        });
        getContentPane().add(tela1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tela1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tela1MousePressed
        // Captura a posição inicial do mouse.
        xInicial = evt.getX();
        yInicial = evt.getY();
    }//GEN-LAST:event_tela1MousePressed

    private void tela1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tela1MouseReleased
        // Captura a posição final do mouse.
        int xFinal = evt.getX();
        int yFinal = evt.getY();
        
        // Caso o usuário tenha desenhado a forma de baixo para cima,
        // invertemos os pontos inicial e final, para a forma ser
        // desenhada corretamente.        
        if (xFinal < xInicial) {
            int aux = xInicial;
            xInicial = xFinal;
            xFinal = aux;
        }
        
        if (yFinal < yInicial) {
            int aux = yInicial;
            yInicial = yFinal;
            yFinal = aux;
        }
        
        int largura = xFinal - xInicial;
        int altura = yFinal - yInicial;
        
        if (rbCirculo.isSelected())
            tela1.desenharForma(new Circulo(xInicial, yInicial, largura, altura));
        else if (rbRetangulo.isSelected())
            tela1.desenharForma(new Retangulo(xInicial, yInicial, largura, altura));
        else if (rbTriangulo.isSelected())
            tela1.desenharForma(new Triangulo(xInicial, yInicial, largura, altura));
    }//GEN-LAST:event_tela1MouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormasAppOCP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormasAppOCP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormasAppOCP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormasAppOCP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormasAppOCP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButton rbCirculo;
    private javax.swing.JRadioButton rbRetangulo;
    private javax.swing.JRadioButton rbTriangulo;
    private formasappocp.Tela tela1;
    // End of variables declaration//GEN-END:variables
}
