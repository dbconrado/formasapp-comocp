package formasappocp;

import java.awt.Graphics;

/**
 *
 * @author daniel.conrado
 */
public class Retangulo extends Forma {
    
    private int x;
    private int y;
    private int largura;
    private int altura;

    public Retangulo(int x, int y, int largura, int altura) {
        this.x = x;
        this.y = y;
        this.largura = largura;
        this.altura = altura;
    }

    @Override
    public void desenhar(Graphics g) {
        g.drawRect(x, y, largura, altura);
    }
    
}
