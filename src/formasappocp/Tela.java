package formasappocp;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author daniel.conrado
 */
public class Tela extends JPanel {

    private List<Forma> formas = new LinkedList<>();
    
    public static Tela fabricarTela() {
        Tela t = new Tela();
        t.setBackground(Color.WHITE);
        return t;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        g.setColor(Color.BLACK);
        
        formas.forEach((forma) -> {
            forma.desenhar(g);
        });
        
    }
    
    public void desenharForma(Forma forma) {
        formas.add(forma);
        this.repaint();
    }
}
