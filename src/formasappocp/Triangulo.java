package formasappocp;

import java.awt.Graphics;

/**
 *
 * @author daniel.conrado
 */
public class Triangulo extends Forma {
    
    private int x;
    private int y;
    private int largura;
    private int altura;

    public Triangulo(int x, int y, int largura, int altura) {
        this.x = x;
        this.y = y;
        this.largura = largura;
        this.altura = altura;
    }

    @Override
    public void desenhar(Graphics g) {
        g.drawPolygon(
                new int[]{x,          x + (largura/2), x + largura},
                new int[]{y + altura, y,               y + altura },
                3);
    }
    
}
