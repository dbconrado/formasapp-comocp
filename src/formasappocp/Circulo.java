package formasappocp;

import java.awt.Graphics;

/**
 *
 * @author daniel.conrado
 */
public class Circulo extends Forma {

    private int x;
    private int y;
    private int largura;
    private int altura;

    public Circulo(int x, int y, int largura, int altura) {
        this.x = x;
        this.y = y;
        this.largura = largura;
        this.altura = altura;
    }
    
    @Override
    public void desenhar(Graphics g) {
        g.drawOval(x, y, largura, altura);
    }
    
}
